const koa=require('koa');
const json=require('koa-json');
const koarouter=require('koa-router');
const path =require('path');
const render=require('koa-ejs');



const app=new koa();
const router=new koarouter();

const dbs=['poriya','ali','mohamad','sajad'];
app.use(json());

// app.use(async ctx =>(ctx.body={ msg:"hello word"}));

render(app,{
    root:path.join(__dirname,'views'),
    layout:'layout',
    viewExt:'html',
    cache:false,
    debug:false
});

router.get('/',async ctx=>{
    await ctx.render('index',{
        title:'all thing is develpoer',
        dbs:dbs,
    });
});



router.get('/test',ctx=>(ctx.body=' i am poriya daliry'));
app.use(router.routes()).use(router.allowedMethods());


app.listen(2525,()=>console.log("server is started"));
